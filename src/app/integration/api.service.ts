import {Injectable, Input} from '@angular/core';
import {filter, map, Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {GetUsersResponse, UserResponse} from "../models/user.model";
import {Info, SearchResponse} from "../models/search.model";

const BASE_URL = 'http://localhost:8080/v3';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient) {
  }

  getUsers(): Observable<UserResponse[]> {
    return this.httpClient.get<GetUsersResponse>(`${BASE_URL}/users/search`).pipe(
      map((users: GetUsersResponse) =>
        users.content.filter((user: UserResponse) => user.email.deliverability === 'DELIVERABLE')));
  }

  checkUser(name: string, email: string): Observable<UserResponse> {
    console.log('checkUser');

    return this.httpClient.post<UserResponse>(
      `${BASE_URL}/users`,
      {
        "name": name,
        "email": email
    });
  }

  searchSimilar(search: string): Observable<SearchResponse> {
    console.log('searchSimilar');
    return this.httpClient.get<SearchResponse>(`${BASE_URL}/search-similars/?search=${search}`);
  }

  getAllSearches(): Observable<Info[]> {
    console.log('getAllSearches');
    return this.httpClient.get<Info[]>(`${BASE_URL}/search-similars/all-searches`);
  }

}
