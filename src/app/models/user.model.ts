export interface GetUsersResponse {
  content: UserResponse[];
}

export interface UserResponse {
  id: string
  name: string
  email: Email
  registrationDate: string
}

export interface Email {
  id: string
  email: string
  deliverability: string
}

export interface UserResponseArray {
  usersArray: [
    {
      id: string,
      name: string,
      email: Email,
      registrationDate: string
    }
  ]
}

