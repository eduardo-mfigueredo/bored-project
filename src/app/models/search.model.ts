export interface SearchResponse {
  Similar: Similar
}

export interface Similar {
  Info: Info[]
  Results: Result[]
}

export interface Info {
  Name: string
  Type: string
}

export interface Result {
  Name: string
  Type: string
  wTeaser: string
}
