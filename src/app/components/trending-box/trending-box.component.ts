import {Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";

@Component({
  selector: 'app-trending-box',
  templateUrl: './trending-box.component.html',
  styleUrls: ['./trending-box.component.scss']
})
export class TrendingBoxComponent implements OnInit {
  @Input() words$!: Observable<string[]>;

  constructor() { }

  ngOnInit(): void {

  }



}
