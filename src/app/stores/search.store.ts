import {Injectable} from "@angular/core";
import {ComponentStore, tapResponse} from "@ngrx/component-store";
import {ApiService} from "../integration/api.service";
import {map, Observable, switchMap} from "rxjs";
import {Info} from "../models/search.model";
import {HttpErrorResponse} from "@angular/common/http";
import {UserResponse} from "../models/user.model";

export interface SearchState {
  trendingSearches: string[];
  validatedEmails: string[];
}


const initialState: SearchState = {
  trendingSearches: ["carregando... "],
  validatedEmails: []
};

@Injectable()
export class SearchStore extends ComponentStore<SearchState> {

  constructor(private apiService: ApiService) {
    super(initialState);
  }

  fetchTrendingSearches = this.effect((trendingFetch$: Observable<void>) => {
    return trendingFetch$.pipe(
      switchMap(() => this.apiService.getAllSearches().pipe(
        tapResponse(
          (response: Info[]) => {
            this.setTrendingSearches([...new Set(
              response.map((info: Info) => info.Name)
            )]);
          },
          (error: HttpErrorResponse) => console.log(error)
        )
      )))
  });

  fetchValidatedEmailList = this.effect((validatedEmailFetch$: Observable<void>) => {
    return validatedEmailFetch$.pipe(
      switchMap(() => this.apiService.getUsers().pipe(
        tapResponse(
          (response: UserResponse[]) => {
            const validatedEmails: string[] = [];
            response.forEach((email) => {
              validatedEmails.push(email.email.email);
            });
            this.setValidatedEmails(validatedEmails);
          },
          (error: HttpErrorResponse) => console.log(error)
        )
      )))
  });

  setTrendingSearches = this.updater(
    (state: SearchState, trendingSearches: string[]) => ({
      ...state, trendingSearches
    }));

  setValidatedEmails = this.updater(
    (state: SearchState, validatedEmails: string[]) => ({
        ...state, validatedEmails
      }
  ));

}
