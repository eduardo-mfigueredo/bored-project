import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ApiService} from "../integration/api.service";
import {Info, Result, SearchResponse} from "../models/search.model";
import {UserResponse, UserResponseArray} from "../models/user.model";
import {SearchStore} from "../stores/search.store";
import {
  debounce,
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  Observable,
  switchMap,
  Subscription,
  tap
} from "rxjs";

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})
export class BodyComponent implements OnInit, AfterViewInit, OnDestroy {
  userForm!: FormGroup;
  searchForm!: FormGroup;
  step: number = 1;
  name: string = '';
  email: string = '';
  search: string = '';
  invalidUser?: string = '';
  results: Result[] | undefined;
  usersEmailList: string[] = [];
  words$ = this.searchStore.select((state) => state.trendingSearches).pipe(
    map((words: string[]) => words.slice(words.length - 20, words.length))
  );
  allWords: string[] = [];
  wordsSubscription!: Subscription;
  filteredOptions?: Observable<string[]>;

  constructor(private apiService: ApiService,
              private searchStore: SearchStore) {
  }

  ngOnInit(): void {
    this.searchStore.fetchTrendingSearches();

    this.searchStore.fetchValidatedEmailList();

    this.userForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
    });

    this.searchForm = new FormGroup({
      search: new FormControl('', [Validators.required])
    });


    // @ts-ignore
    this.filteredOptions = this.searchForm.get('search').valueChanges.pipe(
      map(value => value.trim()),
      filter(value => value.length > 1),
      debounceTime(300),
      distinctUntilChanged(),
      tap((value) => {
        console.log(value)
      }),
      map(value => this._filter(value || '')),
    );


  }

  ngAfterViewInit(): void {
    this.wordsSubscription =
      this.searchStore.select((state) => state.trendingSearches).subscribe((words) => {
        this.allWords = words;
      });
  }

  ngOnDestroy(): void {
    this.wordsSubscription.unsubscribe();
  }

  validateUser(name: string, email: string) {
    if (this.usersEmailList.includes(email)) {
      this.step = this.step + 1;
    } else {
      this.apiService.checkUser(name, email)
        .subscribe((response: any) => {
          response.email.deliverability === 'DELIVERABLE' ? this.step = 2 : this.invalidUser =
            'Oops! Parece que o email não é bom o suficiente :/'
          if (this.invalidUser !== '') {
            this.step = 0;
          }
        });
    }
  }

  searchSimilar(search: string) {
    return this.apiService.searchSimilar(search)
      .subscribe((response: SearchResponse) => {
        this.results = response.Similar.Results;
        console.log(response);
        console.log(this.results);
        this.step = this.step + 1;
      });
  }

  tryAgain() {
    this.step = 1;
    this.invalidUser = '';
    this.name = '';
    this.email = '';
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.allWords.filter(word => word.toLowerCase().startsWith(filterValue));
  }

}
