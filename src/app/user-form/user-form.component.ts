import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ApiService} from "../integration/api.service";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  userForm!: FormGroup;
  @Output() name: string = '';
  @Output() email: string = '';
  @Output() step:  EventEmitter<number> = new EventEmitter<number>();
  usersEmailList: string[] = [];
  @Output() invalidUser?: string = '';


  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.userForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
    });

    this.apiService.getUsers().subscribe((response: any) => {
      console.log(response);

      for (let user of response.content) {
        this.usersEmailList.push(user.email.email);
      }
      console.log(this.usersEmailList);
    });
  }

  validateUser(name: string, email: string) {
    if (this.usersEmailList.includes(email)) {
      this.step.emit(1);
    } else {
      this.apiService.checkUser(name, email)
        .subscribe((response: any) => {
          response.email.deliverability === 'DELIVERABLE' ? this.step.emit(2) : this.invalidUser =
            'Oops! Parece que o email não é bom o suficiente :/'
          if (this.invalidUser !== '') {
            this.step.emit(0);
          }
        });
    }
  }

}
